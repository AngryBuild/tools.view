﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;

namespace Tools
{
    public static class ViewViewModelMatcher
    {
        /// <summary>
        /// Добавить в ресурс набор шаблонов представлений для типов
        /// </summary>
        /// <param name="resouceDictionary">Целевая библиотека ресурсов</param>
        /// <param name="findView">Фильтр типов представлений</param>
        /// <param name="findViewModel">Фильтр типов визуальных моделей</param>
        public static void AddMatch(System.Windows.ResourceDictionary resouceDictionary, Func<Type, bool> findView, Func<Type, Type, bool> findViewModel, Func<Type, bool> filterTypes = null)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var templates = GetMatched(findView, findViewModel);

            AddMatch(resouceDictionary, templates);

            sw.Stop();
            Debug.WriteLine(string.Format("AddMatch worked {0}",sw.Elapsed.ToString()));
        }

        /// <summary>
        /// Добавить в ресурс набор шаблонов представлений для типов
        /// </summary>
        /// <param name="resouceDictionary">Целевая библиотека ресурсов</param>
        /// <param name="findModel">получить тип визуальной модели</param>
        /// <param name="filterTypes">Фильтр анализируемых типов</param>
        public static void AddMatch(System.Windows.ResourceDictionary resouceDictionary, Func<Type, Type> findModel, Func<Type, bool> filterTypes = null)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var templates = GetMatched(findModel, filterTypes);

            AddMatch(resouceDictionary, templates);

            sw.Stop();
            Debug.WriteLine(string.Format("AddMatch worked {0}",sw.Elapsed.ToString()));
        }

        /// <summary>
        /// Добавить в ресурс набор шаблонов представлений для типов
        /// </summary>
        /// <param name="resouceDictionary">Целевая библиотека ресурсов</param>
        /// <param name="templates">Набор шаблонов</param>
        public static void AddMatch(System.Windows.ResourceDictionary resouceDictionary, IEnumerable<KeyValuePair<DataTemplateKey, DataTemplate>> templates)
        {
            foreach (var template in templates)
            {
                resouceDictionary.Add(template.Key, template.Value);
            }
        }

        /// <summary>
        /// Получить набор шаблонов
        /// </summary>
        /// <param name="findView">Фильтр типов представлений</param>
        /// <param name="findViewModel">Фильтр типов визуальных моделей</param>
        public static IEnumerable<KeyValuePair<DataTemplateKey, DataTemplate>> GetMatched(Func<Type, bool> findView, Func<Type, Type, bool> findViewModel, Func<Type, bool> filterTypes = null)
        {
            Stopwatch sw = new Stopwatch();
            Debug.WriteLine(string.Format("GetMatched started"));
            sw.Start();
            var types = GetAllTypes();
            if (filterTypes != null)
                types = types.Where(filterTypes);
            List<Type> viewTypes = new List<Type>();
            List<Type> excludedTypes = new List<Type>();
            foreach (var type in types)
            {
                if (findView(type))
                {
                    viewTypes.Add(type);
                    excludedTypes.Add(type);//исключить сами типы визуальных моджелей, что бы не пытаться прикреплять VM-VM
                }
            }
            Debug.WriteLine(string.Format("GetMatched [{0}] scaned types", sw.Elapsed.ToString()));
            foreach (var type in types)
            {
                if (excludedTypes.Contains(type))
                    continue;

                var typeView = viewTypes.FirstOrDefault(el => findViewModel(type, el));
                if (typeView != null)
                {
                    excludedTypes.Add(typeView);//исключить уже прикрепленные типы
                    var dataTemplate = FormDataTemplate(type, typeView);
                    yield return new KeyValuePair<DataTemplateKey, DataTemplate>(new DataTemplateKey(type), dataTemplate);
                }
            }
            sw.Stop();
            Debug.WriteLine(string.Format("GetMatched [{0}] End work", sw.Elapsed.ToString()));
        }

        /// <summary>
        /// Получить набор шаблонов
        /// </summary>
        /// <param name="findModel">получить тип визуальной модели</param>
        /// <param name="filterTypes">Фильтр анализируемых типов</param>
        public static IEnumerable<KeyValuePair<DataTemplateKey, DataTemplate>> GetMatched(Func<Type, Type> findModel, Func<Type, bool> filterTypes = null)
        {
            Stopwatch sw = new Stopwatch();
            Debug.WriteLine(string.Format("GetMatched started"));
            sw.Start();
            var types = GetAllTypes();
            if (filterTypes != null)
                types = types.Where(filterTypes);
            foreach (var type in types)
            {
                var viewType = type;
                var modelType = findModel(viewType);
                if (modelType!=null)
                {
                    var dataTemplate = FormDataTemplate(modelType, viewType);
                    yield return new KeyValuePair<DataTemplateKey, DataTemplate>(new DataTemplateKey(modelType), dataTemplate);
                }
            }
            sw.Stop();
            Debug.WriteLine(string.Format("GetMatched [{0}] End work", sw.Elapsed.ToString()));
        }


        /// <summary>
        /// Get type by name
        /// </summary>
        /// <param name="typeNeme"></param>
        /// <returns></returns>
        private static Type GetType(string typeNeme)
        {
            Type result = Type.GetType(typeNeme);

            if (result != null)
                return result;

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                result = assembly.GetType(typeNeme);
                if (result != null)
                    break;
            }
            return result;
        }
        /// <summary>
        /// Get all types from all assemblies
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Type> GetAllTypes()
        {
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany(el =>
            {
                IEnumerable<Type> types = new List<Type>();
                try
                {
                    types = el.GetTypes();
                }
                catch (Exception ex)
                {
                    var err = ex;
                }
                return types;
            });
        }

        /// <summary>
        /// Сформировать шаблон для заданного типа танных
        /// </summary>
        /// <param name="typeModel">Тип визуальной модели</param>
        /// <param name="typeView">Тип представления</param>
        /// <returns>Шаблон</returns>
        private static DataTemplate FormDataTemplate(Type typeModel, Type typeView)
        {
            return new DataTemplate
            {
                DataType = typeModel,
                VisualTree = new FrameworkElementFactory(typeView)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool NoSystemTypesFilter(Type type)
        {
            var namesp = type.Namespace;
            if (string.IsNullOrEmpty(namesp))
                return true;
            if (namesp.StartsWith("System"))
                return false;
            if (namesp.StartsWith("Microsoft"))
                return false;
            if (namesp.StartsWith("MS"))
                return false;
            if (namesp.StartsWith("CrystalDecisions"))
                return false;
            if (namesp.StartsWith("Windows"))
                return false;
            return true;
        }
    }
}
