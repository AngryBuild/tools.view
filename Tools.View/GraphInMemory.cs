﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using ZedGraph;

namespace Tools.View
{
    public class GraphInMemory
    {
        public class GraphParams
        {
            public Color LineColor { get; set; }
            public float Width { get; set; }
            public float Height { get; set; }
            public string Title { get; set; }
            public bool IsHideTitle { get; set; }
            public bool IsHideLegend { get; set; }
            public bool IsHideBorder { get; set; }
        }

        public static byte[] MakeGraph(IEnumerable<KeyValuePair<double, double>> data, GraphParams param)
        {
            var chart = new ZedGraph.ZedGraphControl();
            GraphPane pane = chart.GraphPane;
            pane.CurveList.Clear();
            pane.Legend.IsVisible = !param.IsHideLegend;
            pane.XAxis.Title.IsVisible = !param.IsHideLegend;
            pane.X2Axis.Title.IsVisible = !param.IsHideLegend;
            pane.YAxis.Title.IsVisible = !param.IsHideLegend;
            pane.Y2Axis.Title.IsVisible = !param.IsHideLegend;
            pane.Title.IsVisible = !param.IsHideTitle;
            pane.Border.IsVisible = !param.IsHideBorder;
            // Создадим список точек
            PointPairList list = new PointPairList();
            foreach (var point in data)
            {
                list.Add(point.Key, point.Value);
            }
            // Создадим кривую
            pane.AddCurve(param.Title, list, param.LineColor, SymbolType.Plus);
            // Обновим оси и перерисуем график
            chart.AxisChange();
            chart.Invalidate();
            var bmp = ToBitmap(chart, param.Width, param.Height);
            //bmp.Save("temp.bmp");
            return ImageToByte(bmp);
        }

        private static Bitmap ToBitmap(ZedGraphControl chart, float widthPx, float heightPx)
        {

            using (var g = chart.CreateGraphics())
            {
                chart.MasterPane.ReSize(g, new RectangleF(0, 0, widthPx, heightPx));
            }
            var bmp = chart.MasterPane.GetImage();
            return bmp;
        }

        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
    }
}
