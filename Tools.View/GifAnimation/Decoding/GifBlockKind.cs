namespace Tools.View.GifAnimation.Decoding
{
    internal enum GifBlockKind
    {
        Control,
        GraphicRendering,
        SpecialPurpose,
        Other
    }
}
